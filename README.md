# P1 - Laboratório de Desenvolvimento de Aplicativos Nativos

Este projeto utiliza Docker para orquestrar um ambiente de desenvolvimento que inclui uma API Laravel e um frontend React Native com Expo para visualização web.

## Pré-requisitos

Para rodar este projeto, você precisará ter o Docker e o Docker Compose instalados em sua máquina. As instruções de instalação para diferentes sistemas operacionais podem ser encontradas nos links abaixo:

- [Instalar Docker](https://docs.docker.com/get-docker/)
- [Instalar Docker Compose](https://docs.docker.com/compose/install/)

## Estrutura do Projeto

O projeto está dividido em duas partes principais:

- **Backend (Laravel)**: API Restful que gerencia dados e autenticação.
- **Frontend (React Native)**: Interface do usuário construída com React Native e Expo, projetada para ser visualizada na web.

## Como Executar

Para iniciar o projeto, siga os passos abaixo:

1. **Clone o Repositório**:
   ```bash
   git clone https://gitlab.com/mateuss.ms470/p1-react-native.git
   cd p1-react-native
   chmod -R 777 .
   ```

2. **Inicie os Serviços com Docker Compose**:
   Para construir e iniciar os serviços definidos no `docker-compose.yml`, execute:
   ```bash
   docker compose up
   ```

   Este comando irá construir e iniciar todos os contêineres necessários para o projeto. Espere um momento até que todos os serviços estejam rodando.

3. **Acessando a Aplicação**:
   Após todos os serviços estarem em execução, você pode acessar a interface do usuário do React Native via navegador no seguinte endereço:
   ```
   http://localhost:19006
   ```

   A API Laravel pode ser acessada através da porta 8000:
   ```
   http://localhost:8000
   ```

## Comandos Básicos do Docker Compose

Aqui estão alguns comandos básicos do Docker Compose que podem ser úteis para gerenciar os contêineres do projeto:

- **Iniciar os Serviços**:
  ```bash
  docker compose up
  ```
  Use a opção `-d` para rodar em modo detached (fundo):
  ```bash
  docker compose up -d
  ```

- **Parar os Serviços**:
  ```bash
  docker compose down
  ```

- **Ver Logs dos Serviços**:
  Para ver os logs de um serviço específico, use:
  ```bash
  docker compose logs nome_do_servico_ou_id
  ```

- **Reconstruir Serviços**:
  Se você fizer alterações nas dependências do projeto, reconstrua os serviços com:
  ```bash
  docker compose up --build
  ```

---
Boa sorte e feliz codificação! (Professor você é o melhor S2)
