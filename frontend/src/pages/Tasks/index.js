import React, { useState, useEffect } from 'react';
import { View, TextInput, Button, FlatList, Text, TouchableOpacity, StyleSheet } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
const Tasks = ({ navigation }) => {
    const [tasks, setTasks] = useState([]);
    const [currentTask, setCurrentTask] = useState('');
    const [editingId, setEditingId] = useState(null);

    useEffect(() => {
        const fetchTasks = async () => {
            const token = await AsyncStorage.getItem('userToken');
            fetch('http://localhost:8000/api/tasks', {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json',
                },
            })
            .then(response => response.json())
            .then(data => {
                setTasks(data);
            })
            .catch(error => {
                console.error('Erro ao buscar tarefas:', error);
                Alert.alert('Error', 'Não foi possível buscar tarefas.');
            });
        };

        fetchTasks();
    }, []);

    const handleAddOrUpdateTask = async () => {
        const token = await AsyncStorage.getItem('userToken');
        console.log(token);
        const method = editingId ? 'PUT' : 'POST';
        const url = editingId ? `http://localhost:8000/api/tasks/${editingId}` : 'http://localhost:8000/api/tasks';
        
        fetch(url, {
            method: method,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`,
            },
            body: JSON.stringify({
                title: currentTask,
            }),
        })
        .then(response => response.json())
        .then(data => {
            if (method === 'POST') {
                setTasks([...tasks, data]);  // Adicionar a tarefa nova à lista local
            } else {
                setTasks(tasks.map(task => task.id === editingId ? data : task));  // Atualizar a lista local
            }
            setCurrentTask('');
            setEditingId(null);
        })
        .catch(error => {
            alert('Erro ao salvar a tarefa');
            console.error('Erro na Tarefa:', error);
        });
    };

    const handleEdit = (task) => {
        setCurrentTask(task.title);
        setEditingId(task.id);
    };

    const handleDelete = async (id) => {
        const token = await AsyncStorage.getItem('userToken');
        fetch(`http://localhost:8000/api/tasks/${id}`, {
            method: 'DELETE',
            headers: {
                'Authorization': `Bearer ${token}`,
            },
        })
        .then(response => {
            if (response.ok) {
                setTasks(tasks.filter(task => task.id !== id));
            } else {
                alert('Erro ao deletar a tarefa');
            }
        })
        .catch(error => {
            alert('Erro ao deletar a tarefa');
            console.error('Delete Error:', error);
        });
    };

    return (
        <View style={styles.container}>
            <TextInput
                style={styles.input}
                placeholder="Digite a Tarefa"
                value={currentTask}
                onChangeText={setCurrentTask}
                autoFocus={true}
            />
            <Button
                title={editingId ? "Editar Tarefa" : "Adicionar Tarefa"}
                onPress={handleAddOrUpdateTask}
                color="#5cb85c" 
            />
            <FlatList
                data={tasks}
                keyExtractor={(item) => item.id.toString()}
                renderItem={({ item }) => (
                    <View style={styles.item}>
                        <Text style={styles.title}>{item.title}</Text>
                        <View style={styles.buttons}>
                            <Button title="Editar" color="blue" onPress={() => handleEdit(item)} />
                            <Button title="Deletar" color="red" onPress={() => handleDelete(item.id)} />
                        </View>
                    </View>
                )}
                style={styles.list}
            />
        <TouchableOpacity onPress={ async () =>{ await AsyncStorage.removeItem('userToken'); navigation.navigate('login')}} >
            <Text style={styles.ButtonExit} >Sair</Text>
        </TouchableOpacity>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        backgroundColor: '#f5f5f5',  
    },
    input: {
        marginBottom: 20,
        paddingHorizontal: 10,
        paddingVertical: 10,
        borderColor: 'gray',
        borderWidth: 1,
        borderRadius: 5,  
        backgroundColor: 'white',
    },
    item: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 10,
        marginVertical: 5,
        backgroundColor: 'lightgray',
        borderRadius: 5,  
    },
    title: {
        flex: 1,
    },
    buttons: {
        flexDirection: 'row',
        width: 130,  
        justifyContent: 'space-between',
    },
    list: {
        marginTop: 20,
    },
    ButtonExit:{
        color: 'red',
        marginTop: 20,
    }
});

export default Tasks;
