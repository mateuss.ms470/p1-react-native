import React, { useState } from 'react';
import { View, TextInput, Button, StyleSheet, Text, Alert, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

const LoginScreen = ({ navigation }) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');


  const handleLogin = async () => {
    try {
        const response = await fetch('http://localhost:8000/api/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                name: username,
                password: password,
            }),
        });

        const jsonData = await response.json();
        if (response.status === 200) {
            await AsyncStorage.setItem('userToken', jsonData.token);
            navigation.navigate('task');
        } else {
            alert("Login Falhou", "Verifique suas credenciais!");
        }
    } catch (error) {
        console.error('Login Error:', error);
        alert("Login Error", "Entre em contato com o suporte técnico.");
    }
};


  return (
    <View style={styles.container}>
      <Text style={styles.title}>LOGIN</Text>
      <TextInput
        style={styles.input}
        placeholder="Nome"
        value={username}
        onChangeText={setUsername}
      />
      <TextInput
        style={styles.input}
        placeholder="Senha"
        value={password}
        onChangeText={setPassword}
        secureTextEntry={true}
      />
      <Button
        title="Entrar"
        onPress={handleLogin}
      />
      <TouchableOpacity onPress={() => navigation.navigate('createuser')} >
            <Text style={styles.ButtonCreate} >Ainda não possui uma conta ?</Text>
        </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
    backgroundColor: '#ffa500',
  },
  title: {
    color: 'white',
    fontSize: 24,
    marginBottom: 20,
    fontWeight: 'bold',
  },
  input: {
    width: '100%',
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    marginBottom: 20,
    padding: 10,
    backgroundColor: '#fff',

  },
  ButtonCreate: {
    color: 'gray',
    marginTop: 28,
}
});

export default LoginScreen;


