import React, { useState } from 'react';
import { View, Text, TextInput, StyleSheet, Button } from 'react-native';

export default function CreateUser({navigation}){
    const [name, setName] = useState('');
    const [password, setPassword] = useState('');
    const [passwordConfirm, setPasswordConfirm] = useState('');

    function handleCreateUser(){
        if(name && password !== '' && password === passwordConfirm){
            fetch('http://localhost:8000/api/register', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    name: name,
                    password: password,
                }),
            })
            .then(response => response.json())
            .then(data => {
                alert('Cadastro criado com sucesso');
                navigation.navigate('login');
            })
            .catch(error => {
                alert('Erro no cadastro');
                console.error('Registration Error:', error);
            });
        } else {
            alert('Ops! Verifique se todos os campos estão corretos e as senhas coincidem.');
        }
    }
    
    return(
        <View style={styles.Container}>
            <Text style={styles.title}>CADASTRO DE USUÁRIOS</Text>
            <View style={styles.form} >
                <TextInput style={styles.inputUserName} placeholder='Nome'
                autoCompleteType='username' autoCapitalize='none'
                placeholderTextColor='#000' autoCorrect={false}
                onChangeText={(event) => setName(event)}
                 />
                <TextInput style={styles.inputForm} placeholder='Senha'
                autoCompleteType='password' autoCapitalize='none'
                placeholderTextColor='#000' autoCorrect={false}
                onChangeText={(event) => setPassword(event)}
                secureTextEntry={true}
                 />
                <TextInput style={styles.inputForm} placeholder='Confirmar senha'
                autoCompleteType='password' autoCapitalize='none' autoCorrect={false}
                placeholderTextColor='#000'
                onChangeText={(event) => setPasswordConfirm(event)}
                secureTextEntry={true}
                 />
                <Button
                    title="Cadastrar"
                    onPress={handleCreateUser}
                />
            </View>
        </View>
    );
}

export const styles = StyleSheet.create({
    Container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ffa500',
    },
    form: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputUserName: {
        width: '100%',
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        marginBottom: 20,
        padding: 10,   
        backgroundColor: '#fff',

    },
    inputForm: {
        width: '100%',
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        marginBottom: 20,
        padding: 10,
        backgroundColor: '#fff',
    },
    title: {
        color: 'white',
        fontSize: 24,
        marginBottom: 20,
        fontWeight: 'bold',
  },
})
