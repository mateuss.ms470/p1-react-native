<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\AuthController;
use App\Http\Controllers\TaskController;

// Rotas de Autenticação
Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login']);

// Rotas protegidas por autenticação JWT
Route::group(['middleware' => ['jwt.auth']], function() {
    Route::get('user', [AuthController::class, 'getAuthenticatedUser']);
    Route::apiResource('tasks', TaskController::class);
});
