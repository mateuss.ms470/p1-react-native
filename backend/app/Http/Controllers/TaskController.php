<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Task;
use JWTAuth;

class TaskController extends Controller
{
    // Lista todas as tarefas do usuário autenticado
    public function index()
    {
        $user = JWTAuth::parseToken()->authenticate();
        return response()->json($user->tasks);
    }

    // Cria uma nova tarefa
    public function store(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();

        $request->validate([
            'title' => 'required|string|max:255',
        ]);

        $task = new Task([
            'title' => $request->title,
            'user_id' => $user->id,
        ]);
        $task->save();

        return response()->json($task, 201);
    }

    // Exibe uma tarefa específica
    public function show($id)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $task = $user->tasks()->find($id);

        if (!$task) {
            return response()->json(['message' => 'Task not found'], 404);
        }

        return response()->json($task);
    }

    // Atualiza uma tarefa específica
    public function update(Request $request, $id)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $task = $user->tasks()->find($id);

        if (!$task) {
            return response()->json(['message' => 'Task not found'], 404);
        }

        $request->validate([
            'title' => 'required|string|max:255',
        ]);

        $task->title = $request->title;
        $task->save();

        return response()->json($task);
    }

    // Deleta uma tarefa específica
    public function destroy($id)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $task = $user->tasks()->find($id);

        if (!$task) {
            return response()->json(['message' => 'Task not found'], 404);
        }

        $task->delete();

        return response()->json(['message' => 'Task deleted successfully']);
    }
}
